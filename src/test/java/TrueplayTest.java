import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static junit.framework.TestCase.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: georgiy
 * Date: 22.05.18
 * Time: 11:16
 * To change this template use File | Settings | File Templates.
 */
public class TrueplayTest {

    static WebDriver driver = initChromedriver();

    @Before
    public void navigateToLoginPage() {
        driver.get("https://wallet.stage.trueplay.io/login.html");
    }

    @AfterClass
    public static void cleanup() {
        driver.quit();
    }

    @Test
    public void Test1() {
        WebElement reg = driver.findElement(By.id("reg"));
        reg.click();
        WebElement email = driver.findElement(By.id("regForm:emailreg"));
        email.sendKeys("joras@ukr.net");
        WebElement phoneName = driver.findElement(By.id("regForm:phonereg"));
        phoneName.sendKeys("380663836253");
        WebElement password = driver.findElement(By.name("regForm:passwreg"));
        password.sendKeys("12345");
        WebElement passwordRepeat = driver.findElement(By.name("regForm:repasswreg"));
        passwordRepeat.sendKeys("12345");

        WebElement signUpButton = driver.findElement(By.cssSelector("#regForm\\3a signupBtn > span"));
        signUpButton.click();
        WebElement container = driver.findElement(By.id("message_container"));
        String containerText = container.getText();
        assertEquals("Captcha: Validation Error: Value is required.", containerText);
        System.out.println("Captcha: Validation Error: Value is required.");
    }

    @Test
    public void Test2() {
        WebElement reg = driver.findElement(By.id("reg"));
        reg.click();
        WebElement button2 = driver.findElement(By.cssSelector("#regForm\\3a signupBtn > span"));
        button2.click();
        WebElement messageContainer = driver.findElement(By.id("message_container"));
        String messageContainerText = messageContainer.getText();
        assertEquals("Captcha: Validation Error: Value is required.", messageContainerText);
        System.out.println("Captcha: Validation Error: Value is required.");
    }

    @Test
    @Ignore
    public void Test3() {
        WebElement reg = driver.findElement(By.id("reg"));
        reg.click();
        WebElement email = driver.findElement(By.id("regForm:emailreg"));
        email.sendKeys("joras@ukr.net");
        WebElement phoneName = driver.findElement(By.id("regForm:phonereg"));
        phoneName.sendKeys("380663836253");
        WebElement password = driver.findElement(By.name("regForm:passwreg"));
        password.sendKeys("12345");
        WebElement passwordRepeat = driver.findElement(By.name("regForm:repasswreg"));
        passwordRepeat.sendKeys("12345");
        WebElement button3 = driver.findElement(By.xpath("//*[@id=\"recaptcha-anchor\"]/div[5]"));
        button3.click();
    }

    public static WebDriver initChromedriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        return new ChromeDriver();
    }
}